/**
 * Copyright (C) 2014 Mikhail Ivanov
 * tech@locosound.ru
 *
 * This file is part of QInputSwitchIndicator.

    QInputSwitchIndicator is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QInputSwitchIndicator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QInputSwitchIndicator.  If not, see <http://www.gnu.org/licenses/>.
    */
#include "qinputswitchindicator.h"

QInputSwitchIndicator::QInputSwitchIndicator(QWidget *parent)
    : QWidget(parent)
{
    //
    this->setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::WindowMinimizeButtonHint);

    getInputDevice();

    QVBoxLayout *boxLayout = new QVBoxLayout(this);
    QIcon appIcon("://icon.png");
    this->setWindowIcon(appIcon);
    this->setLayout(boxLayout);
    //Create menu
    menu = new QMenu(this);

    //******Dialogs*****//
    //optionsDialog = new QDialog(this);
    //optionsDialog->setModal(true);
    //*****Actions*****//
    //Exit
    QAction* actionExit = new QAction( "Exit", this );
    connect( actionExit, SIGNAL( triggered() ), qApp, SLOT(quit()));
    actionList.append(actionExit);
    //Setup
    //QAction* actionOptions = new QAction("Options", this);
    //connect(actionOptions, SIGNAL(triggered()), optionsDialog, SLOT(exec()));
    QMapIterator<QString, int> i(deviceMap);
    while (i.hasNext()) {
        i.next();

        QAction* action = new QAction(i.key(), this);
        action->setObjectName(i.key());
        action->setCheckable(true);
        action->setChecked(true);

        connect(action, SIGNAL(triggered(bool)), this, SLOT(switchDevice(bool)));
        QPushButton *button = new QPushButton(i.key(), this);
        boxLayout->addWidget(button);
        connect(button, SIGNAL(clicked(bool)), this, SLOT(switchDevice(bool)));

        button->setMinimumSize(100,100);
        button->setObjectName(i.key());
        button->setCheckable(true);
        button->setChecked(true);

        actionList.append(action);

        //Add action to menu
        menu->addAction(action);
    }
    //qDebug() << actionList;
    //Add Action Exit
    menu->addSeparator();
    menu->addAction(actionExit);

    //
    //Add Icon
    QIcon icon = QIcon::fromTheme("go-down");

    //Create trayIcon with icon

    trayIcon = new QSystemTrayIcon(icon);
    trayIcon->setProperty("_sni_qt_category", "ApplicationStatus");
    //trayIcon->show();

    trayIcon->setContextMenu(menu);

    connect( trayIcon, SIGNAL( activated( QSystemTrayIcon::ActivationReason )),
             this, SLOT( iconActivated( QSystemTrayIcon::ActivationReason )));
}

void QInputSwitchIndicator::getInputDevice()
{
    //*****Devices*****//
    QString touchpad = "touchpad";
    QString touchscreen = "touchscreen";
    QRegExp wacomStylus("wacom.*stylus");
    QRegExp wacomEraser("wacom.*eraser");

    //Run xinput
    QProcess xinput;
    xinput.start("xinput");
    if (!xinput.waitForStarted())
        return;
    xinput.waitForFinished(-1);
    QByteArray result = xinput.readAll();

    QString resultStr = result.simplified();
    //resultStr = resultStr.remove("⎡ ").remove("↳ ");
    resultStr = resultStr.replace(QRegExp("(↳|⎡|⎣)"), "|");
    //QRegExp("\[[a-z]*\s[a-z]*\s\([0-9]*\)\]")
    QList<QString> splits = resultStr.toLower().split(" | ");


    if(xinput.exitCode()!=0){
        qDebug () << " Error " << xinput.exitCode() << xinput.readAllStandardError();
    }
    else{
        //qDebug () << " Ok " << splits;
        foreach (QString string, splits) {
            qDebug() << string;
            if(string.contains(touchpad)){
                deviceMap[touchpad] = string.split("=").takeLast().split(" ").takeFirst().toInt();
            }
            else if (string.contains(touchscreen)) {
                deviceMap[touchscreen] = string.split("=").takeLast().split(" ").takeFirst().toInt();
            }
            else if(string.contains(wacomStylus)){
                deviceMap["wacomStylus"] = string.split("=").takeLast().split(" ").takeFirst().toInt();
            }
            else if(string.contains(wacomEraser)){
                deviceMap["wacomEraser"] = string.split("=").takeLast().split(" ").takeFirst().toInt();
            }
        }
        //qDebug() << deviceMap;
    }
    return;
    //qDebug() << result;
}

void QInputSwitchIndicator::iconActivated(QSystemTrayIcon::ActivationReason reason)
{
    switch (reason) {
    case QSystemTrayIcon::Trigger:
        // show your menu here
        menu->show();
        break;
    }
}

void QInputSwitchIndicator::switchDevice(bool value)
{
    QString id;
    id.setNum(deviceMap[sender()->objectName()]);
    //Run xinput
    QProcess xinput;
    if(value){
        xinput.start("xinput", QStringList() << "-enable" << id);
        if (!xinput.waitForStarted())
            return;
        xinput.waitForFinished(-1);
        qDebug() << "Enable:" << sender()->objectName() << id;
    }
    else{
        xinput.start("xinput", QStringList() << "-disable" << id);
        if (!xinput.waitForStarted())
            return;
        xinput.waitForFinished(-1);
        qDebug() << "Disable:" << sender()->objectName() << id;
    }

}

QInputSwitchIndicator::~QInputSwitchIndicator()
{

}
