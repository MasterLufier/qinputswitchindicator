#-------------------------------------------------
# Copyright (C) 2014 Mikhail Ivanov
# tech@locosound.ru
#
# This file is part of QInputSwitchIndicator.
#
#    QInputSwitchIndicator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    QInputSwitchIndicator is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with QInputSwitchIndicator.  If not, see <http://www.gnu.org/licenses/>.
#
# Project created by QtCreator 2014-04-17T14:35:05
#
#-------------------------------------------------

QT       += core
QT      += gui
QT      += widgets

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = QInputSwitch-indicator
TEMPLATE = app


SOURCES += main.cpp\
        qinputswitchindicator.cpp

HEADERS  += qinputswitchindicator.h

RESOURCES += \
    resource.qrc

LIBS += -lsni-qt
LIBS += -L/usr/lib/x86_64-linux-gnu/qt4/plugins/systemtrayicon -lsni-qt

OTHER_FILES += \
    README.md \
    COPYING
