/**
 * Copyright (C) 2014 Mikhail Ivanov
 * tech@locosound.ru
 *
 * This file is part of QInputSwitchIndicator.

    QInputSwitchIndicator is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    QInputSwitchIndicator is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with QInputSwitchIndicator.  If not, see <http://www.gnu.org/licenses/>.
    */
#ifndef QINPUTSWITCHINDICATOR_H
#define QINPUTSWITCHINDICATOR_H

#include <QWidget>
#include <QDialog>
#include <QtGui>
#include <QMenu>
#include <QSystemTrayIcon>
#include <QDebug>
#include <QProcess>
#include <QPushButton>
#include <QLayout>

class QInputSwitchIndicator : public QWidget
{
    Q_OBJECT

public:
    QInputSwitchIndicator(QWidget *parent = 0);
    QMenu *menu;
    QSystemTrayIcon *trayIcon;

    //QDialog *optionsDialog;
    //Name and ID switchable devices
    QMap<QString, int> deviceMap;
    //List of Action
    QList<QAction *> actionList;
    void getInputDevice();

    ~QInputSwitchIndicator();
public slots:
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
    void switchDevice(bool value);
private:

};

#endif // QINPUTSWITCHINDICATOR_H
